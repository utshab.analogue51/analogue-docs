## Install git in your system 

```
Step 1: Go to https://gitforwindows.org/ (this is for git in windows)
Step 2 : Download git bash from the site 
Step 3 : Install according to instruction
```

## Check get version in your machine
```
Step 1 : Go to command (winkey+R)
Step 2 : type cmd
Step 3 : type git version in cmd

If you see message like ( git version 2.25.0.windows.1 ) congratulations your git system is working !!

```


## Setup Git Account in Gitlab

Go to [gitlab.com](https://gitlab.com/) then create account with provided email address for you.

## Command line instructions

You can also upload existing files from your computer using the instructions below.

## Git global setup
```
git config --global user.name "Full Name"
git config --global user.email "username.analogue51@gmail.com"
```

![alt text](https://i.imgur.com/CMvSnXj.png "Title")

## Setup a new repository
```
git clone https://gitlab.com/{username}/{reponame}.git
eg: git clone https://gitlab.com/username/test.git
```

![alt text](https://i.imgur.com/c2AclUV.png "Git clone image")
## Change directory to your folder name eg: test
```
cd test
```

![alt text](https://i.imgur.com/9NXI7ix.png "Cd test")

## Create your branch according to assign task
```
git branch {branch-name}
eg: git branch my-branch
```
## Add changes to your branch
```
git add . (move your file to stagging environment)
git commit -m "commit detail" (comment to your issue and -m refers the modified files)
```
## Publish your commit to remote repo
```
git push origin {your-branch-name}
eg: git push origin my-branch
```

### Your are find some errors please consult with us
